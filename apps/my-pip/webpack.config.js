// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

const path = require('path');

module.exports = require('../../webpack.config.tpl')({
  name: 'my-pip',
  publicUrlFrag: '/my-pip/',
  srcPath: path.resolve(__dirname, 'src'),
  indexPath: path.resolve(__dirname, 'src', 'index.tsx'),
  htmlPath: path.resolve(__dirname, 'public', 'index.html'),
  rootPath: __dirname,
  tsConfigPath: path.resolve('tsconfig.json'),
});
