// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import CommunicationModule from '@zaaksysteem/communication-module';

export type CommunicationPropsType = {
  userUuid: string;
  caseUuid: string;
  rootPath: string;
};

const Communication: React.ComponentType<CommunicationPropsType> = ({
  rootPath,
  userUuid,
  caseUuid,
}) => (
  <CommunicationModule
    contactUuid={userUuid}
    caseUuid={caseUuid}
    rootPath={rootPath}
    context="pip"
    capabilities={{
      allowSplitScreen: false,
      canAddAttachmentToCase: false,
      canAddSourceFileToCase: false,
      canAddThreadToCase: false,
      canCreateMijnOverheid: false,
      canCreateContactMoment: false,
      canCreatePipMessage: true,
      canCreateEmail: false,
      canCreateNote: false,
      canDeleteMessage: false,
      canImportMessage: false,
      canSelectCase: true,
      canSelectContact: false,
      canFilter: false,
      canOpenPDFPreview: false,
    }}
  />
);

export default Communication;
