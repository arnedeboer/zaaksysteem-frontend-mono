// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import * as i18next from 'i18next';
import { useTranslation } from 'react-i18next';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { Rule } from '@zaaksysteem/common/src/components/form/rules';
import { GetFormDefinitionType } from '../../Information.types';
import { useInformationStyles } from '../../Information.style';
import { SubjectType } from './../../../ContactView.types';

const getTitleSuffix = (subject: SubjectType, t: i18next.TFunction) => {
  if (subject.isActive) return;

  const type = subject.dateOfDeath ? 'passed' : 'inactive';
  const text = t(`common.titleSuffix.${type}`);

  return `(${text})`;
};

type CommonPropsType = {
  subject: SubjectType;
  rules?: Rule[];
  getFormDefinition: GetFormDefinitionType;
};

const Common: React.FunctionComponent<CommonPropsType> = ({
  subject,
  rules,
  getFormDefinition,
}) => {
  const [t] = useTranslation('contactView');
  const classes = useInformationStyles();
  const formDefinition = getFormDefinition(t, subject);
  const validationMap = generateValidationMap(formDefinition);

  const {
    fields,
    formik: { values },
  } = useForm({
    rules,
    formDefinition,
    validationMap,
  });

  return (
    <>
      <SubHeader
        title={t('common.title')}
        description={t('common.subTitle')}
        titleSuffix={getTitleSuffix(subject, t)}
        titleSuffixClass={classes.titleSuffix}
      />
      <div className={classes.formWrapper}>
        {fields.map(
          ({ FieldComponent, name, error, touched, value, ...rest }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
      </div>
    </>
  );
};

export default Common;
