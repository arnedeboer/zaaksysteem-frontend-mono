// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import fecha from 'fecha';
import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  hideFields,
  showFields,
} from '@zaaksysteem/common/src/components/form/rules';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from './../../../ContactView.types';

const transformDate = (date: string) =>
  date ? fecha.format(new Date(date), 'DD-MM-YYYY') : null;

export const getCommonFormDefinition = (
  t: i18next.TFunction,
  person: SubjectType
): AnyFormDefinitionField[] => {
  return [
    {
      name: 'firstNames',
      type: fieldTypes.TEXT,
    },
    {
      name: 'insertions',
      type: fieldTypes.TEXT,
    },
    {
      name: 'familyName',
      type: fieldTypes.TEXT,
    },
    {
      name: 'surname',
      type: fieldTypes.TEXT,
    },
    {
      name: 'gender',
      type: fieldTypes.TEXT,
      value: t(`common.person.genderValue.${person.gender}`),
    },
    {
      name: 'dateOfBirth',
      type: fieldTypes.TEXT,
      value: transformDate(person['dateOfBirth']),
    },
    {
      name: 'dateOfDeath',
      type: fieldTypes.TEXT,
      value: transformDate(person['dateOfDeath']),
    },
    {
      name: 'country',
      type: fieldTypes.TEXT,
      value: person['country'] || t('common.person.countryValue.netherlands'),
    },
    {
      name: 'residenceStreet',
      type: fieldTypes.TEXT,
    },
    {
      name: 'residenceHouseNumber',
      type: fieldTypes.TEXT,
    },
    {
      name: 'residenceHouseNumberLetter',
      type: fieldTypes.TEXT,
    },
    {
      name: 'residenceHouseNumberSuffix',
      type: fieldTypes.TEXT,
    },
    {
      name: 'residenceZipcode',
      type: fieldTypes.TEXT,
    },
    {
      name: 'residenceCity',
      type: fieldTypes.TEXT,
    },
    {
      name: 'hasCorrespondenceAddress',
      type: fieldTypes.TEXT,
      value: t(
        `common.person.hasCorrespondenceAddressValue.${person['hasCorrespondenceAddress']}`
      ),
    },
    {
      name: 'correspondenceStreet',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceHouseNumber',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceHouseNumberLetter',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceHouseNumberSuffix',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceZipcode',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceCity',
      type: fieldTypes.TEXT,
    },
    {
      name: 'hasForeignAddress',
      type: fieldTypes.TEXT,
      hidden: true,
    },
    {
      name: 'foreignAddress1',
      type: fieldTypes.TEXT,
    },
    {
      name: 'foreignAddress2',
      type: fieldTypes.TEXT,
    },
    {
      name: 'foreignAddress3',
      type: fieldTypes.TEXT,
    },
  ].map(field => ({
    label: t(`common.person.${field.name}`),
    value: person[field.name],
    readOnly: true,
    ...field,
  }));
};

const residenceAddressFields = [
  'residenceStreet',
  'residenceHouseNumber',
  'residenceHouseNumberLetter',
  'residenceHouseNumberSuffix',
  'residenceZipcode',
  'residenceCity',
];

const correspondenceAddressFields = [
  'correspondenceStreet',
  'correspondenceHouseNumber',
  'correspondenceHouseNumberLetter',
  'correspondenceHouseNumberSuffix',
  'correspondenceZipcode',
  'correspondenceCity',
];

const localAddressFields = [
  ...residenceAddressFields,
  'hasCorrespondenceAddress',
  ...correspondenceAddressFields,
];

const foreignAddressFields = [
  'foreignAddress1',
  'foreignAddress2',
  'foreignAddress3',
];

export const commonRules = [
  new Rule()
    .when('hasForeignAddress', field => Boolean(field.value))
    .then(hideFields(localAddressFields))
    .then(showFields(foreignAddressFields))
    .else(showFields(localAddressFields))
    .else(hideFields(foreignAddressFields)),
  new Rule()
    .when('hasCorrespondenceAddress', field => field.value === 'Ja')
    .then(showFields(correspondenceAddressFields))
    .else(hideFields(correspondenceAddressFields)),
];
