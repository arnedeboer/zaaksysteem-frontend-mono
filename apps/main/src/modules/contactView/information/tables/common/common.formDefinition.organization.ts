// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import {
  Rule,
  hideFields,
  showFields,
} from '@zaaksysteem/common/src/components/form/rules';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType } from './../../../ContactView.types';

const transformDate = (date: string) =>
  date ? date.split('-').reverse().join('-') : null;

export const getCommonFormDefinition = (
  t: i18next.TFunction,
  organization: SubjectType
): AnyFormDefinitionField[] => {
  return [
    {
      name: 'name',
      type: fieldTypes.TEXT,
    },
    {
      name: 'rsin',
      type: fieldTypes.TEXT,
    },
    {
      name: 'oin',
      type: fieldTypes.TEXT,
    },
    {
      name: 'cocNumber',
      type: fieldTypes.TEXT,
    },
    {
      name: 'cocLocationNumber',
      type: fieldTypes.TEXT,
    },
    {
      name: 'organizationType',
      type: fieldTypes.FLATVALUE_SELECT,
      // these values are saved in dutch
      choices: [
        'Eenmanszaak',
        'Maatschap',
        'Vennootschap onder firma',
        'Commanditaire vennootschap met een beherend vennoot',
        'Besloten vennootschap met gewone structuur',
        'Naamloze vennootschap met gewone structuur',
        'Europese naamloze vennootschap (SE) met gewone structuur',
        'Vereniging van eigenaars',
        'Kerkgenootschap',
        'Stichting',
        'Publiekrechtelijke rechtspersoon',
        'Coöperatie',
        'Vereniging',
      ].map(value => ({ value, label: value })),
    },
    {
      name: 'hasCorrespondenceAddress',
      type: fieldTypes.TEXT,
      value: t(
        `common.organization.hasCorrespondenceAddressValue.${organization['hasCorrespondenceAddress']}`
      ),
    },
    {
      name: 'locationCountry',
      type: fieldTypes.TEXT,
    },
    {
      name: 'locationStreet',
      type: fieldTypes.TEXT,
    },
    {
      name: 'locationHouseNumber',
      type: fieldTypes.TEXT,
    },
    {
      name: 'locationHouseNumberLetter',
      type: fieldTypes.TEXT,
    },
    {
      name: 'locationHouseNumberSuffix',
      type: fieldTypes.TEXT,
    },
    {
      name: 'locationZipcode',
      type: fieldTypes.TEXT,
    },
    {
      name: 'locationCity',
      type: fieldTypes.TEXT,
    },
    {
      name: 'locationForeignAddress1',
      type: fieldTypes.TEXT,
    },
    {
      name: 'locationForeignAddress2',
      type: fieldTypes.TEXT,
    },
    {
      name: 'locationForeignAddress3',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceCountry',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceStreet',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceHouseNumber',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceHouseNumberLetter',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceHouseNumberSuffix',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceZipcode',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceCity',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceForeignAddress1',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceForeignAddress2',
      type: fieldTypes.TEXT,
    },
    {
      name: 'correspondenceForeignAddress3',
      type: fieldTypes.TEXT,
    },
    {
      name: 'dateRegistered',
      type: fieldTypes.TEXT,
      value: transformDate(organization['dateRegistered']),
    },
    {
      name: 'dateFounded',
      type: fieldTypes.TEXT,
      value: transformDate(organization['dateFounded']),
    },
    {
      name: 'dateCeased',
      type: fieldTypes.TEXT,
      value: transformDate(organization['dateCeased']),
    },
    {
      name: 'mainActivity',
      type: fieldTypes.TEXT,
    },
    {
      name: 'secondaryActivities',
      type: fieldTypes.MULTI_VALUE_TEXT,
      value: organization['secondaryActivities'].map(
        ({ description }: { description: string }) => ({
          value: description,
          label: description,
        })
      ),
    },
    {
      name: 'contactInitials',
      type: fieldTypes.TEXT,
    },
    {
      name: 'contactInsertions',
      type: fieldTypes.TEXT,
    },
    {
      name: 'contactFamilyName',
      type: fieldTypes.TEXT,
    },
  ].map(field => ({
    label: t(`common.organization.${field.name}`),
    value: organization[field.name],
    readOnly: true,
    ...field,
  }));
};

const locationAddressFields = [
  'locationStreet',
  'locationHouseNumber',
  'locationHouseNumberLetter',
  'locationHouseNumberSuffix',
  'locationZipcode',
  'locationCity',
];

const locationForeignAddressFields = [
  'locationForeignAddress1',
  'locationForeignAddress2',
  'locationForeignAddress3',
];

const correspondenceAddressFields = [
  'correspondenceStreet',
  'correspondenceHouseNumber',
  'correspondenceHouseNumberLetter',
  'correspondenceHouseNumberSuffix',
  'correspondenceZipcode',
  'correspondenceCity',
];

const correspondenceForeignAddressFields = [
  'correspondenceForeignAddress1',
  'correspondenceForeignAddress2',
  'correspondenceForeignAddress3',
];

const allCorrespondenceAddressFields = [
  ...correspondenceAddressFields,
  ...correspondenceForeignAddressFields,
];

export const commonRules = [
  new Rule()
    .when('locationCountry', field => field.value === 'Nederland')
    .then(showFields(locationAddressFields))
    .then(hideFields(locationForeignAddressFields))
    .else(showFields(locationForeignAddressFields))
    .else(hideFields(locationAddressFields)),
  new Rule()
    .when('hasCorrespondenceAddress', field => field.value === 'Ja')
    .then(showFields(['correspondenceCountry']))
    .else(hideFields(['correspondenceCountry'])),
  new Rule()
    .when('correspondenceCountry', field => field.value === 'Nederland')
    .then(showFields(correspondenceAddressFields))
    .then(hideFields(correspondenceForeignAddressFields))
    .else(showFields(correspondenceForeignAddressFields))
    .else(hideFields(correspondenceAddressFields)),
  new Rule()
    .when('hasCorrespondenceAddress', field => field.value === 'Nee')
    .then(hideFields(allCorrespondenceAddressFields)),
];
