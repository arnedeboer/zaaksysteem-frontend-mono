// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType, SessionType } from './../../../ContactView.types';

export type FormValuesType = {
  insideMunicipality: string;
  authenticated: string;
  source: string;
  uuid: string;
};

export const getSpecialFormDefinition = (
  t: i18next.TFunction,
  person: SubjectType,
  session?: SessionType
): AnyFormDefinitionField[] => {
  const allowedToViewSensitiveContactData = session?.capabilities.includes(
    'view_sensitive_contact_data'
  );

  return [
    {
      name: 'personalNumber',
      type: fieldTypes.PERSONAL_NUMBER,
      config: {
        uuid: person.uuid,
        allowedToView: allowedToViewSensitiveContactData,
      },
    },
    {
      name: 'insideMunicipality',
      type: fieldTypes.TEXT,
      readOnly: true,
      value: t(
        `special.insideMunicipalityValue.${person['insideMunicipality']}`
      ),
    },
    {
      name: 'authenticated',
      type: fieldTypes.TEXT,
    },
    {
      name: 'source',
      type: fieldTypes.TEXT,
    },
    {
      name: 'uuid',
      type: fieldTypes.TEXT,
    },
  ].map(field => ({
    label: t(`special.${field.name}`),
    value: person[field.name],
    readOnly: true,
    ...field,
  }));
};
