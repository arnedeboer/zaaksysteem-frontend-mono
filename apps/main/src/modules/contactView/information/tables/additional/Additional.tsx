// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import * as i18next from 'i18next';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { SubHeader } from '@zaaksysteem/common/src/components/SubHeader/SubHeader';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source/object';
import Button from '@mintlab/ui/App/Material/Button';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { saveAdditionalInfo } from '../../requests';
import { useInformationStyles } from '../../Information.style';
import { SubjectType, SessionType } from './../../../ContactView.types';

export interface AdditionalPropsType {
  subject: SubjectType;
  getFormDefinition: ({
    t,
    subject,
    session,
    isAdmin,
  }: {
    t: i18next.TFunction;
    subject: any;
    session?: SessionType;
    isAdmin?: boolean;
  }) => AnyFormDefinitionField[];
  refreshSubject: () => {};
}

const Additional: React.FunctionComponent<AdditionalPropsType> = ({
  subject,
  getFormDefinition,
  refreshSubject,
}) => {
  const isAdmin = useSelector((state: any) =>
    (state.session?.data?.logged_in_user?.capabilities || []).includes('admin')
  );

  const [t] = useTranslation('contactView');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const classes = useInformationStyles();
  const formDefinition = getFormDefinition({ t, subject, isAdmin });
  const validationMap = generateValidationMap(formDefinition);
  const [busy, setBusy] = useState(false);

  const {
    fields,
    formik: { values },
  } = useForm({
    formDefinition,
    validationMap,
  });

  return (
    <>
      {ServerErrorDialog}
      <SubHeader
        title={t('additional.title')}
        description={t('additional.subTitle')}
      />
      <div className={classes.formWrapper}>
        {fields.map(
          ({
            FieldComponent,
            name,
            error,
            touched,
            defaultValue,
            value,
            ...rest
          }) => {
            const restValues = {
              ...cloneWithout(rest, 'type', 'classes'),
              disabled: values.completed,
            };

            return (
              <FormControlWrapper
                {...restValues}
                error={error}
                touched={touched}
                key={name}
              >
                <FieldComponent
                  name={name}
                  value={value || defaultValue || value}
                  key={name}
                  {...restValues}
                />
              </FormControlWrapper>
            );
          }
        )}
        {(isAdmin || (!isAdmin && !subject.anonymousUser)) && (
          <Button
            action={() => {
              setBusy(true);
              saveAdditionalInfo(subject.uuid, values, subject.type)
                .then(() => {
                  refreshSubject();
                })
                .catch(openServerErrorDialog)
                .finally(() => setBusy(false));
            }}
            className={classes.button}
            presets={['contained', 'primary']}
            disabled={busy}
          >
            {t('save')}
          </Button>
        )}
      </div>
    </>
  );
};

export default Additional;
