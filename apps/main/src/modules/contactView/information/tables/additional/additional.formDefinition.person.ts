// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import * as i18next from 'i18next';
import * as fieldTypes from '@zaaksysteem/common/src/components/form/constants/fieldTypes';
import { AnyFormDefinitionField } from '@zaaksysteem/common/src/components/form/types';
import { SubjectType, SessionType } from './../../../ContactView.types';

type getChoiceWithTranslationType = (
  t: i18next.TFunction,
  values: string
) => { value: string; label: string };

const getChoiceWithTranslation: getChoiceWithTranslationType = (t, value) => ({
  value,
  label: t(`additional.preferredContactChannelValue.${value}`),
});

export type FormValuesType = {
  phoneNumber: string;
  mobileNumber: string;
  email: string;
  preferredContactChannel: string;
  internalNote: string;
  anynymousUser: boolean;
};

export const getAdditionalFormDefinition = ({
  t,
  subject,
  isAdmin = false,
}: {
  t: i18next.TFunction;
  subject: SubjectType;
  session?: SessionType;
  isAdmin?: boolean;
}): AnyFormDefinitionField[] => {
  const { anonymousUser } = subject;
  return [
    {
      name: 'phoneNumber',
      type: fieldTypes.PHONE_NUMBER,
      readOnly: anonymousUser,
    },
    {
      name: 'mobileNumber',
      type: fieldTypes.PHONE_NUMBER,
      readOnly: anonymousUser,
    },
    {
      name: 'email',
      type: fieldTypes.EMAIL,
      readOnly: anonymousUser,
    },
    {
      name: 'preferredContactChannel',
      type: fieldTypes.FLATVALUE_SELECT,
      defaultValue: 'pip',
      choices: ['pip', 'email', 'mail', 'phone'].map(value =>
        getChoiceWithTranslation(t, value)
      ),
      readOnly: anonymousUser,
    },
    {
      name: 'internalNote',
      type: fieldTypes.TEXT,
      readOnly: anonymousUser,
    },
    {
      name: 'anonymousUser',
      type: fieldTypes.CHECKBOX,
      applyBackgroundColor: false,
      hidden: !isAdmin,
    },
  ].map(field => ({
    label: t(`additional.${field.name}`),
    value: subject[field.name],
    ...field,
  }));
};
