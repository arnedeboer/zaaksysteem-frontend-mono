// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';
import { Theme } from '@mintlab/ui/types/Theme';

export const useInformationStyles = makeStyles(
  ({
    palette: {
      danger: { main },
    },
  }: Theme) => ({
    wrapper: {
      padding: 20,
      boxSizing: 'border-box',
    },
    titleSuffix: {
      color: main,
      marginLeft: '5px',
    },
    formWrapper: {
      padding: 20,
    },
    buttonWrapper: {
      display: 'flex',
    },
    button: {
      marginTop: 20,
      marginRight: 20,
    },
  })
);
