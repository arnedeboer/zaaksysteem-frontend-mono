// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { SubjectType } from '../../ContactView.types';

export const transformPerson = (person: SubjectType) => {
  const {
    id: uuid,
    type,
    attributes: {
      source,
      authenticated,
      noble_title: nobleTitle,
      first_names: firstNames,
      insertions,
      family_name: familyName,
      surname,
      gender,
      date_of_birth: dateOfBirth,
      date_of_death: dateOfDeath,
      foreign_address: hasForeignAddress,
      inside_municipality,
      residence_address,
      correspondence_address,
      contact_information,
      is_active: isActive,
    },
    relationships,
  } = person;

  const residenceStreet = residence_address?.street;
  const residenceHouseNumber = residence_address?.street_number;
  const residenceHouseNumberLetter = residence_address?.street_number_letter;
  const residenceHouseNumberSuffix = residence_address?.street_number_suffix;
  const residenceZipcode = residence_address?.zipcode;
  const residenceCity = residence_address?.city;

  const correspondenceStreet = correspondence_address?.street;
  const correspondenceHouseNumber = correspondence_address?.street_number;
  const correspondenceHouseNumberLetter =
    correspondence_address?.street_number_letter;
  const correspondenceHouseNumberSuffix =
    correspondence_address?.street_number_suffix;
  const correspondenceZipcode = correspondence_address?.zipcode;
  const correspondenceCity = correspondence_address?.city;
  const country = correspondence_address?.country;

  const foreignAddress1 = correspondence_address?.address_line_1;
  const foreignAddress2 = correspondence_address?.address_line_2;
  const foreignAddress3 = correspondence_address?.address_line_3;

  const hasCorrespondenceAddress =
    !hasForeignAddress && Boolean(correspondence_address) ? 'yes' : 'no';

  const insideMunicipality = inside_municipality ? 'yes' : 'no';

  const email = contact_information?.email;
  const mobileNumber = contact_information?.mobile_number;
  const phoneNumber = contact_information?.phone_number;
  const internalNote = contact_information?.internal_note;
  const anonymousUser = contact_information?.is_an_anonymous_contact_person;
  const preferredContactChannel =
    contact_information?.preferred_contact_channel;

  const relatedObjectUuid = relationships?.related_custom_object?.data?.id;

  return {
    uuid,
    type,
    source,
    authenticated,
    anonymousUser,
    nobleTitle,
    firstNames,
    insertions,
    familyName,
    surname,
    gender,
    dateOfBirth,
    dateOfDeath,
    hasForeignAddress,
    country,
    insideMunicipality,
    residenceStreet,
    residenceHouseNumber,
    residenceHouseNumberLetter,
    residenceHouseNumberSuffix,
    residenceZipcode,
    residenceCity,
    hasCorrespondenceAddress,
    correspondenceStreet,
    correspondenceHouseNumber,
    correspondenceHouseNumberLetter,
    correspondenceHouseNumberSuffix,
    correspondenceZipcode,
    correspondenceCity,
    foreignAddress1,
    foreignAddress2,
    foreignAddress3,
    email,
    mobileNumber,
    phoneNumber,
    internalNote,
    preferredContactChannel,
    relatedObjectUuid,
    isActive,
  };
};
