// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { fetchContactInformation } from './Information.actions';
import { useInformationStyles } from './Information.style';
import Person from './views/Person';
import Organization from './views/Organization';
import Employee from './views/Employee';
import { SessionType, SubjectType } from './../ContactView.types';

const TableDictionary: { [key: string]: any } = {
  person: Person,
  organization: Organization,
  employee: Employee,
};

type InformationPropsType = {
  subject: SubjectType;
  session: SessionType;
  refreshSubject: () => {};
};

const Information: React.FunctionComponent<InformationPropsType> = ({
  subject,
  session,
  refreshSubject,
}) => {
  const classes = useInformationStyles();
  const View = TableDictionary[subject.type];
  const altAuthActive = session.activeInterfaces.includes('auth_twofactor');

  return (
    <div className={classes.wrapper}>
      <DataProvider
        provider={fetchContactInformation}
        providerArguments={[subject, altAuthActive]}
        autoProvide={true}
      >
        {({ data, busy }) => {
          if (busy) {
            return <Loader />;
          }

          return (
            data && (
              <View
                data={data}
                session={session}
                refreshSubject={refreshSubject}
              />
            )
          );
        }}
      </DataProvider>
    </div>
  );
};

export default Information;
