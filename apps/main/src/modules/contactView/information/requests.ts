// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { SubjectTypeType } from '@zaaksysteem/common/src/types/SubjectTypes';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { FormikValues } from 'formik';

export const checkHasPhoneIntegration = async () => {
  const result = await request(
    'GET',
    '/api/v1/sysin/interface/get_by_module_name/kcc'
  );

  return result.result.instance.pager.rows === 1;
};

export const fetchRelatedObject = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectRequestParams>(
    '/api/v2/cm/custom_object/get_custom_object',
    {
      uuid,
    }
  );
  const result = await request('GET', url).catch(() => {
    // for when the user is given the uuid of the related object in the get_contact call
    // but is not allowed to know anything about the object
    return null;
  });

  return result?.data;
};

export const fetchObjectType = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetCustomObjectTypeRequestParams>(
    `/api/v2/cm/custom_object_type/get_custom_object_type`,
    {
      uuid,
    }
  );
  const result = await request('GET', url);

  return result.data || [];
};

export const fetchSubjectSettings = async (uuid: string) => {
  const url = buildUrl<APICaseManagement.GetContactSettingsRequestParams>(
    `/api/v2/cm/contact/get_settings`,
    {
      uuid,
    }
  );
  const result =
    await request<APICaseManagement.GetContactSettingsResponseBody>('GET', url);

  return result.data || [];
};

/* eslint complexity: [2, 8] */
export const saveAdditionalInfo = async (
  uuid: string,
  values: FormikValues,
  type: string
) => {
  const url = '/api/v2/cm/contact/save_contact_info';

  const result =
    await request<APICaseManagement.ChangePhoneExtensionResponseBody>(
      'POST',
      url,
      {
        uuid,
        type,
        contact_information: {
          phone_number: values.phoneNumber || '',
          mobile_number: values.mobileNumber || '',
          email: values.email || '',
          preferred_contact_channel: values.preferredContactChannel || '',
          internal_note: values.internalNote || '',
          is_an_anonymous_contact_person: values.anonymousUser || false,
        },
      }
    );

  return result;
};

export const savePhoneExtension = async (uuid: string, extension: string) => {
  const url = '/api/v2/cm/contact/change_phone_extension';

  const result =
    await request<APICaseManagement.ChangePhoneExtensionResponseBody>(
      'POST',
      url,
      {
        uuid,
        extension,
      }
    );

  return result;
};

export const uploadSignatureAction = async (
  subjectUuid: string,
  fileUuid: string
) => {
  const url = '/api/v2/cm/contact/save_signature';

  const result = await request<APICaseManagement.SaveSignatureRequestBody>(
    'POST',
    url,
    {
      uuid: subjectUuid,
      file_uuid: fileUuid,
    }
  );

  return result;
};

export const deleteSignatureAction = async (uuid: string) => {
  const url = '/api/v2/cm/contact/delete_signature';

  const result = await request<APICaseManagement.DeleteSignatureRequestBody>(
    'POST',
    url,
    {
      uuid,
    }
  );

  return result;
};

export const saveNotificationSettings = async (
  uuid: string,
  settings: FormikValues
) => {
  const url = '/api/v2/cm/contact/set_notification_settings';

  const result =
    await request<APICaseManagement.SetNotificationSettingsRequestBody>(
      'POST',
      url,
      {
        uuid,
        notification_settings: {
          new_document: Boolean(settings.newDocument),
          new_assigned_case: Boolean(settings.newAssignedCase),
          case_term_exceeded: Boolean(settings.caseTermExceeded),
          new_ext_pip_message: Boolean(settings.newExtPipMessage),
          new_attribute_proposal: Boolean(settings.newAttributeProposal),
          case_suspension_term_exceeded: Boolean(
            settings.caseSuspensionTermExceeded
          ),
        },
      }
    );

  return result;
};

type AltAuthResponseBodyType = {
  result: {
    id: number;
    type: SubjectTypeType;
    is_active: boolean;
    username: string;
    phone_number: string;
  }[];
};

export const fetchAltAuthIntegration = async () => {
  const result = await request(
    'GET',
    '/api/v1/sysin/interface/get_by_module_name/auth_twofactor'
  );

  return result.result.instance.rows[0].instance;
};

export const fetchAltAuth = async (uuid: string) => {
  const result: AltAuthResponseBodyType = await request(
    'GET',
    `/betrokkene/get_alternative_auth/${uuid}`
  );

  return result.result[0];
};

export const saveAltAuth = async (id: number, data: any) =>
  await request(
    'POST',
    `/betrokkene/generate_alternative_authentication/${id}`,
    data
  );

export const sendAltAuth = async (id: number, data: any) =>
  await request('POST', `/betrokkene/send_activation_link/${id}`, data);
