// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { SizeInfo } from 'react-virtualized';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import { useCaseTableStyles } from './Cases.style';

const CaseTableLoader = ({
  loading,
  tableDimensions,
}: {
  loading: boolean;
  tableDimensions: SizeInfo | null;
}) => {
  const classes = useCaseTableStyles();
  if (!loading || !tableDimensions || tableDimensions?.height < 10) return null;
  return (
    <div
      className={classes.loader}
      style={{
        top: tableDimensions.height - 150,
      }}
    >
      <Loader />
    </div>
  );
};

export default CaseTableLoader;
