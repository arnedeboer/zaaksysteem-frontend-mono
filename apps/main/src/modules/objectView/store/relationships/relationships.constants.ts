// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';

export const FETCH_CASES = createAjaxConstants('FETCH_CASES');
export const FETCH_OBJECTS = createAjaxConstants('FETCH_OBJECTS');
export const FETCH_SUBJECTS = createAjaxConstants('FETCH_SUBJECTS');
