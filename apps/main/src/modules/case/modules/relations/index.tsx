// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import Relations from './components/Relations';
import locale from './locale/relations.locale';
import { SessionType, CaseV1Type, CaseTypeV1Type } from './../../types';

export type RelationsModulePropsType = {
  session: SessionType;
  caseUuid: string;
  caseObjV1: CaseV1Type;
  caseOpen: boolean;
  caseTypeV1: CaseTypeV1Type;
};

const RelationsModule: React.ComponentType<
  RelationsModulePropsType
> = props => (
  <I18nResourceBundle resource={locale} namespace="caseRelations">
    <Relations {...props} />
  </I18nResourceBundle>
);

export default RelationsModule;
