// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CaseTypeV1Type } from '../../../../../types';
import { ObjectType } from '../../../types/Relations.types';
import { fetchObjects } from '../requests';

type GetCasesType = (
  caseUuid: string,
  caseNumber: number,
  setObjects: (objects: ObjectType[]) => void
) => void;

export const getObjects: GetCasesType = async (
  caseUuid,
  caseNumber,
  setObjects
) => {
  const response = await fetchObjects(caseNumber);
  const objects = response.map(({ object_class_name, uuid, name }: any) => ({
    uuid,
    name,
    type: object_class_name,
  }));

  setObjects(objects);
};

type GetChoicesType = (
  caseTypeV1: CaseTypeV1Type
) => { label: string; value: string }[];

export const getChoices: GetChoicesType = caseTypeV1 => {
  const phases = caseTypeV1.phases;
  const fields = phases.reduce(
    (acc: any[], phase: any) => [...acc, ...phase.fields],
    []
  );
  const objectFields = fields.filter((field: any) => field.type === 'object');
  const choices = objectFields
    .filter(({ object_metadata: { relate_object } }: any) => relate_object)
    .map(({ label, object_type_prefix }: any) => ({
      label,
      value: object_type_prefix,
    }));

  return choices;
};
