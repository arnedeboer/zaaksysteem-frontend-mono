// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect, useState } from 'react';
import {
  CaseRelationType,
  CaseTablesPropsType,
} from '../../../types/Relations.types';
import FamilyTable from './FamilyTable';
import RelatedTable from './RelatedTable';
import { getCases, refreshRelatedCases } from './library';

const CaseTables: React.ComponentType<CaseTablesPropsType> = ({
  caseUuid,
  caseObjV1,
}) => {
  const [family, setFamily] = useState<CaseRelationType[]>([]);
  const [related, setRelated] = useState<CaseRelationType[]>([]);

  useEffect(() => {
    getCases(caseUuid, caseObjV1, setFamily, setRelated);
  }, []);

  return (
    <>
      <FamilyTable cases={family} />
      <RelatedTable
        cases={related}
        caseUuid={caseUuid}
        refresh={() => refreshRelatedCases(caseUuid, setRelated)}
      />
    </>
  );
};

export default CaseTables;
