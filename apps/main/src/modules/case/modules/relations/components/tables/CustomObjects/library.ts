// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CustomObjectType } from '../../../types/Relations.types';
import { fetchCustomObjects } from '../requests';

type GetCustomObjectsType = (
  caseUuid: string,
  setCustomObjects: (customObjects: CustomObjectType[]) => void
) => void;

export const getCustomObjects: GetCustomObjectsType = async (
  caseUuid,
  setCustomObjects
) => {
  const response = await fetchCustomObjects(caseUuid);
  const customObjects = response.data.map(
    ({ id, attributes: { name, title } }) => ({
      uuid: id,
      // this is not a mistake. An object is based on an object type
      // title is the name of the specific object
      // name is the name of the object type
      name: title,
      type: name,
    })
  );

  setCustomObjects(customObjects);
};
