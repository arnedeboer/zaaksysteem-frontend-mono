// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { RelationsPropsType } from '../types/Relations.types';
import { useRelationsStyles } from './Relations.style';
import CaseTables from './tables/Cases/CaseTables';
import PlannedCasesTable from './tables/PlannedCases/PlannedCasesTable';
import SubjectsTable from './tables/Subjects/SubjectsTable';
import ObjectsTable from './tables/Objects/ObjectsTable';
import CustomObjectsTable from './tables/CustomObjects/CustomObjectsTable';
import PlannedEmailsTable from './tables/PlannedEmails/PlannedEmailsTable';

const Relations: React.ComponentType<RelationsPropsType> = ({
  session,
  caseUuid,
  caseObjV1,
  caseOpen,
  caseTypeV1,
}) => {
  const classes = useRelationsStyles();
  const {
    configurable: { show_object_v1, show_object_v2 },
    active_interfaces,
  } = session;
  const showPlannedEmails = active_interfaces.includes('email');

  return (
    <div className={classes.wrapper}>
      <CaseTables caseUuid={caseUuid} caseObjV1={caseObjV1} />
      <PlannedCasesTable
        caseUuid={caseUuid}
        caseNumber={caseObjV1.number}
        caseOpen={caseOpen}
      />
      <SubjectsTable caseUuid={caseUuid} caseOpen={caseOpen} />
      {show_object_v1 && (
        <ObjectsTable
          caseUuid={caseUuid}
          caseNumber={caseObjV1.number}
          caseOpen={caseOpen}
          caseTypeV1={caseTypeV1}
        />
      )}
      {show_object_v2 && (
        <CustomObjectsTable caseUuid={caseUuid} caseOpen={caseOpen} />
      )}
      {showPlannedEmails && (
        <PlannedEmailsTable caseNumber={caseObjV1.number} caseOpen={caseOpen} />
      )}
    </div>
  );
};

export default Relations;
