// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { CaseTypeV1Type, CaseV1Type, SessionType } from './../../../types';

type TablesPropsType = {
  session: SessionType;
  caseUuid: string;
  caseNumber: number;
  caseObjV1: CaseV1Type;
  caseOpen: boolean;
  caseTypeV1: CaseTypeV1Type;
};

export type CaseTablesPropsType = Pick<
  TablesPropsType,
  'caseUuid' | 'caseObjV1'
>;

export type RelationsPropsType = {
  session: SessionType;
  caseUuid: string;
  caseObjV1: CaseV1Type;
  caseOpen: boolean;
  caseTypeV1: CaseTypeV1Type;
};

export type PlannedCasesTablePropsType = Pick<
  TablesPropsType,
  'caseUuid' | 'caseNumber' | 'caseOpen'
>;

export type SubjectTablePropsType = Pick<
  TablesPropsType,
  'caseUuid' | 'caseOpen'
>;

export type ObjectsTablePropsType = Pick<
  TablesPropsType,
  'caseUuid' | 'caseOpen' | 'caseNumber' | 'caseTypeV1'
>;

export type CustomObjectsTablePropsType = Pick<
  TablesPropsType,
  'caseUuid' | 'caseOpen'
>;

export type PlannedEmailsTablePropsType = Pick<
  TablesPropsType,
  'caseNumber' | 'caseOpen'
>;

export type LevelType = 'A' | 'B' | 'C';

export type RelationType =
  | 'initiator'
  | 'continuation'
  | 'plain'
  | 'child'
  | 'parent'
  | 'self';

export interface CaseRelationType
  extends Omit<
    CaseV1Type,
    | 'status'
    | 'phase'
    | 'requestor'
    | 'assignee'
    | 'caseType'
    | 'htmlEmailTemplateName'
  > {
  relation_type: RelationType;
  case_uuid: string;
  level?: LevelType;
  assignee: string;
  casetype_title: string;
  sequence_number: number;
}

export type PlannedEmailType = {
  uuid: string;
  name: string;
  date: string;
  template: string;
  recipient: string;
};

export type PlannedCaseType = {
  uuid: string;
  name: string;
  casetype_uuid: string;
  casetype_title: string;
  next_run: string;
  runs_left: string;
  interval_value: string;
  interval_period: string;
};

export type SubjectType = {
  uuid: string;
  name: string;
  subject: {
    uuid: string;
    // type: 'person' | 'organization' | 'employee';
    type: string;
    name: string;
  };
  role: string;
  authorized: boolean;
  magic_string_prefix: string;
  permission: 'none' | 'search' | 'read' | 'write';
};

export type ObjectType = {
  uuid: string;
  type: string;
  name: string;
};

export type CustomObjectType = {
  uuid: string;
  type: string;
  name: string;
};
