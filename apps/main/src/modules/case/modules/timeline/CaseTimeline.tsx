// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.
import React from 'react';
import { useTranslation } from 'react-i18next';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import Button from '@mintlab/ui/App/Material/Button';
import Timeline from '@zaaksysteem/common/src/components/Timeline/Timeline';
import { getData, getExportFunction } from './CaseTimeline.library';
import { useCaseTimelineStyles } from './CaseTimeline.styles';

const ContactTimeline: React.FunctionComponent<any> = ({
  caseUuid,
  caseId,
}) => {
  const classes = useCaseTimelineStyles();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const getDataFunction = getData({ openServerErrorDialog, uuid: caseUuid });
  const [t] = useTranslation('');

  return (
    <div className={classes.wrapper}>
      <div className={classes.scrollWrapper}>
        {ServerErrorDialog}
        <Button
          action={() =>
            //@ts-ignore
            (top.window.location.href = `/intern/zaak/${caseId}/timeline/`)
          }
          presets={['primary']}
        >
          Timeline V1
        </Button>
        <Timeline
          getData={getDataFunction}
          exportFunction={getExportFunction(caseUuid)}
          filtersOptions={[
            {
              label: t('common:timeline.filters.changes'),
              value: 'case_update',
              checked: false,
            },
            {
              label: t('common:timeline.filters.documents'),
              value: 'document',
              checked: false,
            },
          ]}
        />
      </div>
    </div>
  );
};

export default ContactTimeline;
