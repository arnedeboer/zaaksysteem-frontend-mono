// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useCaseTimelineStyles = makeStyles(() => ({
  wrapper: {
    height: '100%',
    overflowY: 'scroll',
  },
  scrollWrapper: {
    height: '100%',
    margin: 16,
  },
}));

export type ClassesType = ReturnType<typeof useCaseTimelineStyles>;
