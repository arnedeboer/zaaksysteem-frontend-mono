// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import CaseTimeline from './CaseTimeline';
import locale from './CaseTimeline.locale';

export type TimelineModulePropsType = {
  caseUuid: string;
  caseId: string;
};

const RelationsModule: React.ComponentType<TimelineModulePropsType> = props => (
  <I18nResourceBundle resource={locale} namespace="caseRelations">
    <CaseTimeline {...props} />
  </I18nResourceBundle>
);

export default RelationsModule;
