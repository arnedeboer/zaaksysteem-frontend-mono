// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

/* eslint react/no-danger: 0 */
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';
//@ts-ignore
import { cloneWithout } from '@mintlab/kitchen-sink/source';
import FormControlWrapper from '@mintlab/ui/App/Zaaksysteem/FormHelpers/FormControlWrapper';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import ConfirmDialog from '@zaaksysteem/common/src/components/ConfirmDialog/ConfirmDialog';
import generateCustomFieldFormDefinition from '@zaaksysteem/common/src/components/form/library/generateCustomFieldFormDefinition';
import generateCustomFieldValues from '@zaaksysteem/common/src/components/form/library/generateCustomFieldValues';
import formatGetCaseCustomFields from '@zaaksysteem/common/src/components/form/library/formatGetCaseCustomFields';
import { generateValidationMap } from '@zaaksysteem/common/src/components/form/validation/library/generateValidationMap';
import { Button } from '@mintlab/ui/App/Material/Button';
import { useForm } from '@zaaksysteem/common/src/components/form/hooks/useForm';
import { isAdminSelector } from '@zaaksysteem/common/src/store/session/session.selectors';
import { ObjectTypeType, ObjectType } from '../types/ObjectForm.types';
import { SessionType, SystemRolesType } from '../../../types';
import { createObject, updateObject, deactivateObject } from './library';
import { useObjectFormStyles } from './ObjectForm.style';
import {
  CaseType,
  CaseTypeV1Type,
  CaseV1ResponseBodyType,
} from './../../../types';
import { getObjectTypeAndObject, mapCustomFields } from './library';

export interface ObjectFormPropsType {
  session: SessionType;
  caseObj: CaseType;
  caseV1response: CaseV1ResponseBodyType;
  caseTypeV1: CaseTypeV1Type;
  objectUuid: string;
  objectTypeUuid: string;
  attributeId: string;
  systemRoles: SystemRolesType;
  type: 'create' | 'update';
}

type ObjectFormInnerProps = {
  object?: ObjectType;
  objectType: ObjectTypeType;
} & Pick<
  ObjectFormPropsType,
  | 'session'
  | 'caseObj'
  | 'caseV1response'
  | 'caseTypeV1'
  | 'attributeId'
  | 'systemRoles'
  | 'type'
>;

/* eslint complexity: [2, 17] */
const InnerObjectForm: React.FunctionComponent<ObjectFormInnerProps> = ({
  objectType,
  object,
  caseObj,
  caseV1response,
  caseTypeV1,
  attributeId,
  systemRoles,
  type,
}) => {
  const updateMode = type === 'update';
  const mappedFields = mapCustomFields(
    caseObj,
    caseTypeV1,
    attributeId,
    systemRoles
  );
  const isAdmin = useSelector(isAdminSelector);
  const isObjectTypeAdmin = objectType.authorizations.includes('admin');
  const isAllowedSystemAttributes = isAdmin || isObjectTypeAdmin;

  const customFieldsValues =
    object?.customFieldsValues ||
    (!updateMode ? formatGetCaseCustomFields(mappedFields) : {});

  const formDefinition = generateCustomFieldFormDefinition({
    customFieldsDefinition: objectType.customFieldsDefinition,
    customFieldsValues,
    config: {
      context: {
        type: 'CaseObjectForm' as const,
        data: {
          magic_string: '',
          case: caseV1response,
          caseType: caseV1response.casetype,
          objectType: objectType || null,
          object: object || null,
        },
      },
    },
  });
  formDefinition.forEach(field => {
    field.config = {
      ...field.config,
      context: {
        type: 'CaseObjectForm' as const,
        data: {
          ...field.config.context.data,
          magic_string: field.name,
        },
      },
    };
    if (isAllowedSystemAttributes) {
      field.hidden = false;
    }
  });
  const validationMap = generateValidationMap(formDefinition);

  const {
    fields,
    formik: { isValid, values },
  } = useForm({
    isInitialValid: true,
    enableReinitialize: true,
    formDefinition,
    validationMap,
  });

  const classes = useObjectFormStyles();
  const [t] = useTranslation('objectForm');
  const [showConfirm, setShowConfirm] = useState(false);

  if (updateMode && !object) {
    throw new Error('Cannot enter update mode without object instance');
  } else if (
    updateMode &&
    object &&
    object.status === 'inactive' &&
    object.authorizations.includes('admin')
  ) {
    const isAdmin = object.authorizations.includes('admin');

    return (
      <div className={classes.wrapper}>
        <p className={classes.inactiveWarning}>
          {isAdmin ? t('inactiveWarning.admin') : t('inactiveWarning.normal')}
        </p>
      </div>
    );
  } else {
    return (
      <div className={classes.wrapper}>
        <div className={classes.scrollWrapper}>
          {fields.map(
            ({ FieldComponent, name, error, touched, value, ...rest }) => {
              const restValues = {
                ...cloneWithout(rest, 'type', 'classes'),
                disabled: values.completed,
              };

              return (
                <FormControlWrapper
                  {...restValues}
                  compact={true}
                  error={error}
                  touched={touched}
                  key={name}
                >
                  <FieldComponent
                    name={name}
                    value={value}
                    key={name}
                    {...restValues}
                  />
                </FormControlWrapper>
              );
            }
          )}

          <div className={classes.actionWrapper}>
            <Button
              presets={['contained', 'medium', 'primary']}
              className={classes.button}
              action={() =>
                updateMode && object
                  ? updateObject(
                      object.relatedCasesUuids || [],
                      object.uuid,
                      generateCustomFieldValues(values, formDefinition)
                    )
                  : createObject(
                      objectType.versionUuid,
                      generateCustomFieldValues(values, formDefinition)
                    )
              }
              disabled={!isValid}
            >
              {t(`form.${type}`)}
            </Button>
            {updateMode && object && (
              <Button
                presets={['contained', 'medium']}
                className={classes.button}
                color="danger"
                action={() => setShowConfirm(true)}
              >
                {t('form.deactivate')}
              </Button>
            )}
            {updateMode && object && (
              <ConfirmDialog
                open={showConfirm}
                onConfirm={() => {
                  deactivateObject(
                    object.relatedCasesUuids || [],
                    object.uuid,
                    generateCustomFieldValues(values, formDefinition)
                  );
                  setShowConfirm(false);
                }}
                onClose={() => setShowConfirm(false)}
                title={t('confirm.deactivate.title')}
                body={<div>{t('confirm.deactivate.description')}</div>}
              />
            )}
          </div>
        </div>
      </div>
    );
  }
};

export const ObjectForm: React.ComponentType<ObjectFormPropsType> = ({
  session,
  caseObj,
  caseV1response,
  caseTypeV1,
  objectUuid,
  objectTypeUuid,
  attributeId,
  systemRoles,
  type,
}) => {
  const [object, setObject] = useState<ObjectType>();
  const [objectType, setObjectType] = useState<ObjectTypeType>();

  useEffect(() => {
    getObjectTypeAndObject(
      setObjectType,
      objectTypeUuid,
      setObject,
      objectUuid
    );
  }, []);

  const isUpdateMode = type === 'update';

  return !caseObj ||
    !caseV1response ||
    !objectType ||
    (isUpdateMode && !object) ? (
    <Loader />
  ) : (
    <InnerObjectForm
      session={session}
      caseObj={caseObj}
      caseV1response={caseV1response}
      caseTypeV1={caseTypeV1}
      objectType={objectType}
      object={object}
      attributeId={attributeId}
      systemRoles={systemRoles}
      type={type}
    />
  );
};

export default ObjectForm;
