// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import useMessages from '@zaaksysteem/common/src/library/useMessages';
import ObjectForm, { ObjectFormPropsType } from './components/ObjectForm';
import locale from './locale/objectForm.locale';

type ObjectFormModulePropsType = ObjectFormPropsType;

const ObjectFormModule: React.ComponentType<ObjectFormModulePropsType> = ({
  session,
  objectTypeUuid,
  objectUuid,
  caseObj,
  caseV1response,
  caseTypeV1,
  attributeId,
  systemRoles,
  type,
}) => {
  const [t] = useTranslation('objectForm');
  const [, addMessages, removeMessages] = useMessages();

  useEffect(() => {
    const messages: { [key: string]: any } = t('serverErrors', {
      returnObjects: true,
    });

    addMessages(messages);

    return () => removeMessages(messages);
  }, []);

  return (
    <I18nResourceBundle resource={locale} namespace="objectForm">
      <ObjectForm
        session={session}
        caseObj={caseObj}
        caseV1response={caseV1response}
        caseTypeV1={caseTypeV1}
        objectUuid={objectUuid}
        objectTypeUuid={objectTypeUuid}
        attributeId={attributeId}
        systemRoles={systemRoles}
        type={type}
      />
    </I18nResourceBundle>
  );
};

export default ObjectFormModule;
