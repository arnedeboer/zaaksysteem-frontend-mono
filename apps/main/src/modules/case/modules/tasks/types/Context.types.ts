// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export interface TasksContextType {
  caseUuid: string;
  rootPath: string;
  phase: number;
}
