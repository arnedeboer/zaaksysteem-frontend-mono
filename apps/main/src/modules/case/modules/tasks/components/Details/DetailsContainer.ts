// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { connect } from 'react-redux';
import { Dispatch } from 'redux';
import {
  NestedFormValue,
  PartialFormValuesType,
} from '@zaaksysteem/common/src/components/form/types/formDefinition.types';
import { RouteComponentProps } from 'react-router-dom';
//@ts-ignore
import { filterProperties } from '@mintlab/kitchen-sink/source';
import mapValuesToFormDefinition from '@zaaksysteem/common/src/components/form/library/mapValuesToFormDefinition';
import {
  editTask,
  deleteTask,
  setCompletion,
} from '../../store/tasks/tasks.actions';
import { CaseTask } from '../../types/List.types';
import { TasksRootStateType } from '../../store/tasks.reducer';
import detailsFormDefinition from '../../fixtures/detailsFormDefinition';
import { Details, DetailsPropsType } from './Details';

type PropsFromDispatchType = Pick<
  DetailsPropsType,
  'deleteAction' | 'editAction' | 'setCompletionAction'
>;

type PropsFromStateType =
  | (Pick<
      DetailsPropsType,
      'rootPath' | 'formDefinition' | 'is_editable' | 'can_set_completion'
    > & { task_uuid: CaseTask['task_uuid'] })
  | undefined;

type PropsFromOwnType = {
  match: RouteComponentProps<{
    task_uuid: CaseTask['task_uuid'];
  }>;
};

const mapStateToProps = (
  {
    tasks: {
      list: { data },
      context,
    },
  }: TasksRootStateType,
  {
    match: {
      match: {
        params: { task_uuid },
      },
    },
  }: PropsFromOwnType
): PropsFromStateType => {
  const task: CaseTask | undefined = data.find(
    (thisTask: CaseTask): Boolean => thisTask.task_uuid === task_uuid
  );

  if (!task) return;

  const formValues: PartialFormValuesType<CaseTask> = {
    title: task.title,
    description: task.description,
    due_date: task.due_date,
    assignee: task.assignee,
    completed: task.completed,
  };

  return {
    rootPath: context.rootPath,
    task_uuid,
    formDefinition: mapValuesToFormDefinition<CaseTask>(
      formValues,
      detailsFormDefinition
    ),
    is_editable: task.is_editable,
    can_set_completion: task.can_set_completion,
  };
};

const mapDispatchToProps = (
  dispatch: Dispatch,
  {
    match: {
      match: {
        params: { task_uuid },
      },
    },
  }: PropsFromOwnType
): PropsFromDispatchType => {
  return {
    editAction(values: Partial<CaseTask>) {
      const sanitizeAssignee = (assignee: NestedFormValue | undefined | null) =>
        assignee
          ? {
              label: assignee.label,
              value: assignee.value,
            }
          : null;

      const dispatchValues = {
        ...filterProperties(values, 'description', 'title', 'due_date'),
        assignee: sanitizeAssignee(values.assignee),
        task_uuid,
      };
      dispatch(editTask(dispatchValues) as any);
    },
    setCompletionAction(completed: boolean) {
      dispatch(setCompletion(task_uuid, completed as boolean) as any);
    },
    deleteAction() {
      dispatch(deleteTask(task_uuid) as any);
    },
  };
};

export const DetailsContainer = connect<
  PropsFromStateType,
  PropsFromDispatchType,
  PropsFromOwnType,
  TasksRootStateType
>(
  mapStateToProps,
  mapDispatchToProps
)(Details);

export default DetailsContainer;
