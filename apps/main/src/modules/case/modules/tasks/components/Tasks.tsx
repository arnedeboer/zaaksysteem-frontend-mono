// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import { TaskListContainer } from './Tasks/TaskList/TaskListContainer';
import DetailsContainer from './Details/DetailsContainer';

export interface TasksPropsType {
  rootPath: string;
}

export const Tasks: React.ComponentType<TasksPropsType> = ({ rootPath }) => {
  return (
    <Switch>
      <Route exact path={`${rootPath}`} render={() => <TaskListContainer />} />
      <Route
        exact
        path={`${rootPath}/edit/:task_uuid`}
        render={match => <DetailsContainer match={match} />}
      />
    </Switch>
  );
};
