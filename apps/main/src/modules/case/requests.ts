// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { buildUrl } from '@mintlab/kitchen-sink/source';
import { request } from '@zaaksysteem/common/src/library/request/request';
import { APICaseManagement } from '@zaaksysteem/generated';
import { CaseV1ResponseBodyType, SessionResponseBodyType } from './types';

export const fetchSession = async () => {
  const response = await request<SessionResponseBodyType>(
    'GET',
    '/api/v1/session/current'
  );

  return response.result.instance;
};

export const fetchCase = async (uuid: string) => {
  const response = await request<APICaseManagement.GetCaseBasicResponseBody>(
    'GET',
    buildUrl('/api/v2/cm/case/get_case', { case_uuid: uuid })
  );

  return response;
};

export const fetchCaseV1 = async (uuid: string) => {
  const response = await request<CaseV1ResponseBodyType>(
    'GET',
    `/api/v1/case/${uuid}`
  );

  return response.result.instance;
};

export const fetchCaseType = async (uuid: string) => {
  const response =
    await request<APICaseManagement.GetCaseTypeVersionResponseBody>(
      'GET',
      buildUrl<APICaseManagement.GetCaseTypeVersionRequestParams>(
        '/api/v2/cm/case_type/get_case_type_version',
        { version_uuid: uuid }
      )
    );

  return response;
};

export const fetchCaseTypeV1 = async (uuid: string, version: number) => {
  const response =
    await request<APICaseManagement.GetCaseTypeActiveVersionResponseBody>(
      'GET',
      buildUrl(`/api/v1/casetype/${uuid}`, { version })
    );

  return response.result.instance;
};

export const fetchSubject = async (
  type: 'person' | 'organization' | 'employee',
  uuid: string
) => {
  const response = await request<APICaseManagement.GetContactResponseBody>(
    'GET',
    buildUrl<APICaseManagement.GetContactRequestParams>(
      '/api/v2/cm/contact/get_contact',
      { type, uuid }
    )
  );

  return response;
};
