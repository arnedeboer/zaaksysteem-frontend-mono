// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';

type CapabilitiesType = 'admin'[];

export type SessionResponseBodyType = {
  result: {
    instance: {
      configurable: {
        show_object_v1: boolean;
        show_object_v2: boolean;
      };
      active_interfaces: string[];
      logged_in_user: {
        capabilities: CapabilitiesType;
      };
    };
  };
};
export type SessionType = SessionResponseBodyType['result']['instance'];

export type CaseType = APICaseManagement.GetCaseResponseBody;
export type CaseV1ResponseBodyType = any;

export type CaseV1Type = {
  uuid: string;
  name: string;
  number: number;
  phase: string;
  progress_status: number;
  result: string | null;
  status: string;
  summary: string;
  htmlEmailTemplateName: string;
  requestor: {
    dateCreated: string;
    displayName: string;
    subjectType: string;
    uuid: string;
  };
  assignee: {
    displayName: string;
  };
  caseType: {
    displayName: string;
    uuid: string;
  };
};

export type CaseTypeType = { phases: any; settings: any };
export type CaseTypeV1Type = any;

export type SubjectType = APICaseManagement.GetContactResponseBody;
export type SystemRolesType = {
  assignee?: SubjectType;
  requestor?: SubjectType;
  recipient?: SubjectType;
  coordinator?: SubjectType;
};

export type GetSystemRolesType = (
  caseObj: CaseType
) => Promise<SystemRolesType>;
