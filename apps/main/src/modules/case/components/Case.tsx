// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { Route, Switch } from 'react-router-dom';
import CommunicationModule from '@zaaksysteem/communication-module/src';
//@ts-ignore
import { objectifyParams } from '@mintlab/kitchen-sink/source/url';
import ObjectFormModule from './../modules/objectForm';
import TaskModule from './../modules/tasks';
import RelationsModule from './../modules/relations';
import TimelineModule from './../modules/timeline';
import {
  SessionType,
  CaseType,
  CaseV1Type,
  CaseV1ResponseBodyType,
  CaseTypeType,
  CaseTypeV1Type,
  SystemRolesType,
} from './../types';

export interface CasePropsType {
  session: SessionType;
  caseObj: CaseType;
  caseObjV1: CaseV1Type;
  caseType: CaseTypeType;
  caseTypeV1: CaseTypeV1Type;
  caseV1response: CaseV1ResponseBodyType;
  systemRoles: SystemRolesType;
}

/* eslint complexity: [2, 9] */
const Case: React.ComponentType<CasePropsType> = ({
  session,
  caseObj,
  caseObjV1,
  caseV1response,
  caseType: { settings },
  caseTypeV1,
  systemRoles,
}) => {
  const caseOpen = caseObjV1.status !== 'resolved';
  const canCreatePipMessage =
    caseObjV1.requestor.subjectType !== 'employee' &&
    settings.disable_pip_for_requestor !== true;

  return (
    <React.Fragment>
      <Switch>
        <Route
          path={`/:prefix/case/:caseId/communication`}
          render={({ match }) => (
            <CommunicationModule
              capabilities={{
                allowSplitScreen: true,
                canAddAttachmentToCase: caseOpen,
                canAddSourceFileToCase: caseOpen,
                canAddThreadToCase: false,
                canCreateContactMoment: true,
                canCreatePipMessage: caseOpen && canCreatePipMessage,
                canCreateEmail:
                  process.env.WEBPACK_BUILD_TARGET !== 'production' && caseOpen,
                canCreateNote: true,
                canCreateMijnOverheid: false,
                canDeleteMessage: caseOpen,
                canImportMessage: caseOpen,
                canSelectCase: false,
                canSelectContact: true,
                canFilter: true,
                canOpenPDFPreview: true,
              }}
              context="case"
              caseUuid={caseObjV1.uuid}
              contactUuid={caseObjV1.requestor.uuid}
              contactName={caseObjV1.requestor.displayName}
              htmlEmailTemplateName={caseObjV1.htmlEmailTemplateName}
              rootPath={match.url}
            />
          )}
        />
        <Route
          path={`/:prefix/case/:caseId/phase/:phase/tasks`}
          render={({
            match: {
              params: { caseId, phase },
              url,
            },
          }: any) => (
            <TaskModule caseUuid={caseId} phase={phase} rootPath={url} />
          )}
        />
        <Route
          path={`/:prefix/case/:caseId/relations`}
          render={({
            match: {
              params: { caseId },
            },
          }) => (
            <RelationsModule
              session={session}
              caseUuid={caseId}
              caseObjV1={caseObjV1}
              caseOpen={caseOpen}
              caseTypeV1={caseTypeV1}
            />
          )}
        />
        <Route
          path={`/:prefix/case/:caseId/object/:type(create|update)/:objectUuid?`}
          render={({
            history: {
              location: { search },
            },
            match: {
              params: { type, objectUuid },
            },
          }: any) => {
            const [, queryParam] = search.split('?');
            const { objectTypeUuid, attributeId } = objectifyParams(queryParam);

            return (
              <ObjectFormModule
                type={type}
                session={session}
                caseObj={caseObj}
                caseV1response={caseV1response}
                caseTypeV1={caseTypeV1}
                objectTypeUuid={objectTypeUuid}
                objectUuid={objectUuid}
                attributeId={attributeId}
                systemRoles={systemRoles}
              />
            );
          }}
        />
        <Route
          path={`/:prefix/case/:caseUuid/timeline`}
          render={({
            match: {
              params: { caseUuid },
            },
          }: any) => {
            return (
              <TimelineModule
                caseUuid={caseUuid}
                caseId={caseObj?.data?.attributes.number}
              />
            );
          }}
        />
      </Switch>
    </React.Fragment>
  );
};

export default Case;
