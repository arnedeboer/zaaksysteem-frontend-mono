// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { APICaseManagement } from '@zaaksysteem/generated';
import {
  fetchSession,
  fetchCase,
  fetchCaseV1,
  fetchCaseType,
  fetchCaseTypeV1,
  fetchSubject,
} from './requests';
import {
  CaseV1ResponseBodyType,
  CaseV1Type,
  CaseTypeType,
  GetSystemRolesType,
} from './types';

type GetProgressStatusType = (phases: any[], phase: string | null) => number;

const getProgressStatus: GetProgressStatusType = (phases, currentPhaseName) => {
  if (!phases || !phases.length || !currentPhaseName) {
    return 0;
  }

  const numberOfPhases = phases.length;
  const phaseNames = phases.map(phase => phase.phase);
  const currentPhase = phaseNames.indexOf(currentPhaseName);

  return currentPhase / numberOfPhases;
};

export type ConvertV1CaseType = (
  caseResponse: CaseV1ResponseBodyType,
  caseType: CaseTypeType
) => CaseV1Type;

const convertV1Case: ConvertV1CaseType = (
  {
    requestor: {
      reference,
      instance: { date_created, display_name, subject_type },
    },
    id,
    status,
    number,
    result,
    assignee,
    casetype,
    subject,
    phase,
    html_email_template,
  },
  { phases }
) => ({
  uuid: id,
  name: id,
  number,
  phase,
  progress_status: getProgressStatus(phases, phase),
  result,
  status,
  summary: subject,
  htmlEmailTemplateName: html_email_template,
  requestor: {
    dateCreated: date_created,
    displayName: display_name,
    subjectType: subject_type,
    uuid: reference,
  },
  assignee: {
    displayName: assignee?.preview,
  },
  caseType: {
    displayName: casetype?.preview,
    uuid: casetype.reference,
  },
});

type ConvertCaseTypeType = (
  caseType: APICaseManagement.GetCaseTypeActiveVersionResponseBody
) => CaseTypeType;

const convertCaseType: ConvertCaseTypeType = ({ data }) => ({
  phases: data?.attributes.phases,
  settings: data?.attributes.settings,
});

// recipient is a caseRole, but is not included in the get_case call as one
// it's instead included as a regular related contact (legacy mistake)
const getSystemRoles: GetSystemRolesType = async caseObj => {
  const relationships = caseObj.data?.relationships;
  const caseRoles = ['assignee', 'coordinator', 'requestor' /*, 'recipient' */];
  const subjectPromises = caseRoles.map(async (caseRole: string) => {
    const existsInCase = Object.prototype.hasOwnProperty.call(
      relationships,
      caseRole
    );

    if (!existsInCase) return;

    const subjectRelationship = relationships[caseRole];
    const type = subjectRelationship.data.type;
    const correctedType = type === 'subject' ? 'employee' : type;

    return fetchSubject(correctedType, subjectRelationship.data.id);
  });

  const relatedContacts = relationships.related_contacts;
  const recipientData = (relatedContacts || []).find(
    (contact: any) => contact.meta.role === 'Ontvanger'
  );
  const recipientPromises = recipientData
    ? [fetchSubject(recipientData.data.type, recipientData.data.id)]
    : [];

  const [assignee, coordinator, requestor, recipient] = await Promise.all([
    ...subjectPromises,
    ...recipientPromises,
  ]);

  return {
    assignee,
    requestor,
    recipient,
    coordinator,
  };
};

export const provideForCase = async (caseUuid: string) => {
  const [session, caseObj, caseV1Response] = await Promise.all([
    fetchSession(),
    fetchCase(caseUuid),
    fetchCaseV1(caseUuid),
  ]);

  const caseTypeVersionUuid = caseObj.data?.attributes.case_type_version_uuid;
  const caseTypeUuid = caseV1Response.casetype.reference;
  const caseTypeVersion = caseV1Response.casetype.instance.version;

  const [caseTypeResponse, caseTypeV1] = await Promise.all([
    fetchCaseType(caseTypeVersionUuid),
    fetchCaseTypeV1(caseTypeUuid, caseTypeVersion),
  ]);

  const caseType = convertCaseType(caseTypeResponse);
  const caseObjV1 = convertV1Case(caseV1Response, caseType);

  const systemRoles = await getSystemRoles(caseObj);

  return {
    session,
    caseObj,
    caseObjV1,
    caseV1Response,
    caseType,
    caseTypeV1,
    systemRoles,
  };
};
