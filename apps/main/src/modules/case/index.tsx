// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { RouteComponentProps } from 'react-router';
import I18nResourceBundle from '@zaaksysteem/common/src/components/i18nResourceBundle/I18nResourceBundle';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import { Loader } from '@mintlab/ui/App/Zaaksysteem/Loader';
import Case from './components/Case';
import locale from './locale/case.locale';
import { provideForCase } from './library';

const CaseModule: React.FunctionComponent<
  RouteComponentProps<{
    caseId: string;
  }>
> = ({
  match: {
    params: { caseId },
  },
}) => (
  <I18nResourceBundle resource={locale} namespace="case">
    <DataProvider
      provider={provideForCase}
      providerArguments={[caseId]}
      autoProvide={true}
    >
      {({ data, busy }) => {
        if (busy) {
          return <Loader />;
        }

        return (
          data && (
            <Case
              session={data.session}
              caseObj={data.caseObj}
              caseObjV1={data.caseObjV1}
              caseV1response={data.caseV1Response}
              caseType={data.caseType}
              caseTypeV1={data.caseTypeV1}
              systemRoles={data.systemRoles}
            />
          )
        );
      }}
    </DataProvider>
  </I18nResourceBundle>
);

export default CaseModule;
