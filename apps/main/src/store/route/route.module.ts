// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  connectRouter,
  routerMiddleware,
  RouterRootState,
} from 'connected-react-router';
import { IModule } from 'redux-dynamic-modules';
import { History } from 'history';
import { iframeMiddleware } from './iframe.middleware';

export const getRouterModule = (
  history: History
): IModule<RouterRootState> => ({
  id: 'router',
  reducerMap: {
    router: connectRouter(history) as any,
  },
  middlewares: [routerMiddleware(history), iframeMiddleware],
});

export default getRouterModule;
