// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import Button from '@mintlab/ui/App/Material/Button';
import { useNotificationBarStyles } from './NotificationBar.styles';

type NotificationBarPropsType = {
  notifications: string[];
};

const NotificationBar: React.ComponentType<NotificationBarPropsType> = ({
  notifications,
}) => {
  const classes = useNotificationBarStyles();
  const [notificationList, setNotificationList] = useState(notifications);

  const hideNotification = (notification: string) => {
    const newList = notificationList.filter(item => item !== notification);

    setNotificationList(newList);
  };

  if (!notificationList.length) return null;

  return (
    <div className={classes.wrapper}>
      {notificationList.map((notification, index) => (
        <div key={index} className={classes.notificationWrapper}>
          <span className={classes.text}>{notification}</span>
          <Button
            className={classes.button}
            presets={['icon', 'extraSmall']}
            action={() => hideNotification(notification)}
          >
            close
          </Button>
        </div>
      ))}
    </div>
  );
};

export default NotificationBar;
