// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const UI_DRAWER_OPEN = 'UI:DRAWER:OPEN';
export const UI_DRAWER_CLOSE = 'UI:DRAWER:CLOSE';
export const UI_BANNER_SHOW = 'UI:BANNER:SHOW';
export const UI_BANNER_HIDE = 'UI:BANNER:HIDE';
export const UI_OVERLAY_OPEN = 'UI:OVERLAY:OPEN';
export const UI_OVERLAY_CLOSE = 'UI:OVERLAY:CLOSE';
export const UI_WINDOW_LOAD = 'UI:WINDOW:LOAD';
export const UI_WINDOW_UNLOAD = 'UI:WINDOW:UNLOAD';
