// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { createAjaxConstants } from '../../../../library/redux/ajax/createAjaxConstants';

export const CATALOG_CHANGE_ONLINE_STATUS = createAjaxConstants(
  'CATALOG:CHANGE_ONLINE_STATUS'
);
export const CATALOG_INIT_CHANGE_ONLINE_STATUS =
  'CATALOG:INIT:CHANGE_ONLINE_STATUS';
