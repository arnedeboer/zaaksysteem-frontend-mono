// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import { FormFieldComponentType } from '@zaaksysteem/common/src/components/form/types/Form2.types';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { fetchLocationChoices } from './LocationFinder.library';

export const LocationFinder: FormFieldComponentType<
  ValueType<string>,
  { appointmentIntegrationUuid?: string }
> = props => {
  const appointmentIntegrationUuid = props?.config.appointmentIntegrationUuid;
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  return (
    <React.Fragment>
      <DataProvider
        autoProvide={appointmentIntegrationUuid !== undefined}
        provider={fetchLocationChoices(openServerErrorDialog)}
        providerArguments={[appointmentIntegrationUuid]}
      >
        {({ data, busy, provide }) => {
          const normalizedChoices = data || [];
          return (
            <FlatValueSelect
              {...props}
              choices={normalizedChoices}
              isClearable={true}
              loading={busy}
              getChoices={provide}
              isDisabled={!normalizedChoices.length}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};
