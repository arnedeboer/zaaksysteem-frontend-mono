// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
import FlatValueSelect from '@zaaksysteem/common/src/components/form/fields/FlatValueSelect';
import { FormFieldComponentType } from '@zaaksysteem/common/src/components/form/types/Form2.types';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { ValueType } from '@mintlab/ui/App/Zaaksysteem/Select/types/ValueType';
import { fetchProductChoices } from './ProductFinder.library';

export const ProductFinder: FormFieldComponentType<
  ValueType<string>,
  { appointmentIntegrationUuid: string; locationUuid: string }
> = props => {
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const appointmentId = props.config?.appointmentIntegrationUuid;
  const locationId = props.config?.locationUuid;

  return (
    <React.Fragment>
      <DataProvider
        autoProvide={locationId !== undefined}
        provider={fetchProductChoices(openServerErrorDialog)}
        key={locationId}
        providerArguments={[appointmentId, locationId]}
      >
        {({ data, busy, provide }) => {
          const normalizedChoices = data || [];
          return (
            <FlatValueSelect
              {...props}
              choices={normalizedChoices}
              isClearable={true}
              loading={busy}
              getChoices={provide}
              isDisabled={!normalizedChoices.length}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};
