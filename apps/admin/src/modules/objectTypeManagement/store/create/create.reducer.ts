// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { Reducer } from 'redux';
import {
  AJAX_STATE_INIT,
  AjaxState,
} from '@zaaksysteem/common/src/library/redux/ajax/createAjaxConstants';
import { handleAjaxStateChange } from '@zaaksysteem/common/src/library/redux/ajax/handleAjaxStateChange';
import { AjaxAction } from '@zaaksysteem/common/src/library/redux/ajax/createAjaxAction';
import { OBJECT_TYPE_CREATE } from './create.constants';

export interface CreateStateType {
  state: AjaxState;
}

const initialState: CreateStateType = {
  state: AJAX_STATE_INIT,
};

const ajaxStateHandler = handleAjaxStateChange(OBJECT_TYPE_CREATE);

export const create: Reducer<CreateStateType, AjaxAction> = (
  state = initialState,
  action
) => {
  return ajaxStateHandler(state, action);
};

export default create;
