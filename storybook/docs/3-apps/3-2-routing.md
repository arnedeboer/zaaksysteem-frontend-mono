# Routing

## 🗺️ Routing the user in the correct direction

Routing is handled by the `react-router` and `react-router-dom` package.

## External documentation:

- [`react-router` documentation](https://reacttraining.com/react-router/web/guides/quick-start)
