# Welcome

## 🚀 to Zaaksysteem mono repo

A collection of apps and packages which (combined) expose the Zaaksysteem.nl UI.

This repo uses `yarn` as a NPM module manager, `Lerna` as a monorepo manager and `Storybook` as a styleguide.

## External documentation

- [Lerna](https://github.com/lerna/lerna/blob/master/README.md)
- [Storybook](https://storybook.js.org/)
- [Yarn](https://classic.yarnpkg.com/en/docs/)
