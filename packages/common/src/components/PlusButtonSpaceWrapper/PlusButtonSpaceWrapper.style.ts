// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const usePlusButtonSpaceWrapperStyle = makeStyles({
  root: {
    width: 'inherit',
    '&:after': {
      content: '""',
      height: 80,
      display: 'block',
    },
  },
});
