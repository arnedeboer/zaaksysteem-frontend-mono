// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { DocumentPreviewPropsType } from '../DocumentPreview.types';
import { ViewerControls } from './ViewerControls';
import { useImageViewerStyles } from './ImageViewer.style';

export interface ImageViewerPropsType extends DocumentPreviewPropsType {}

export const ImageViewer: React.ComponentType<ImageViewerPropsType> = ({
  url,
  title,
}) => {
  const classes = useImageViewerStyles();
  const [scale, setScale] = useState(1);

  return (
    <div className={classes.wrapper}>
      <ViewerControls
        className={classes.controls}
        scale={scale}
        onScaleChange={setScale}
      />
      <div className={classes.imageWrapper}>
        <div className={classes.imageScrollWrapper}>
          <img
            className={classes.image}
            alt={title}
            style={{ transform: `scale(${scale})` }}
            src={url}
          />
        </div>
      </div>
    </div>
  );
};

export default ImageViewer;
