// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import {
  FormDefinition,
  AnyFormDefinitionField,
} from '../types/formDefinition.types';
import { isSteppedForm } from './formHelpers';

function removeValuesFromFields<FormShape>(
  fields: AnyFormDefinitionField<FormShape>[]
): AnyFormDefinitionField<FormShape>[] {
  return fields.map(field => ({
    ...field,
    value: null,
  }));
}

export function getFormDefinitionWithoutValues<FormShape>(
  formDefinition: FormDefinition<FormShape>
): FormDefinition<FormShape> {
  return isSteppedForm<FormShape>(formDefinition)
    ? formDefinition.map(step => ({
        ...step,
        fields: removeValuesFromFields(step.fields),
      }))
    : removeValuesFromFields(formDefinition);
}

export default getFormDefinitionWithoutValues;
