// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import MultilineOption from '@mintlab/ui/App/Zaaksysteem/Select/Option/MultilineOption';
import { Select, ValueType } from '@mintlab/ui/App/Zaaksysteem/Select';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormFieldComponentType } from '../../types/Form2.types';
import { fetchObjectTypeChoices } from './ObjectTypeFinder.library';

const ObjectTypeFinder: FormFieldComponentType<ValueType<string>> = props => {
  const [input, setInput] = useState('');
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();

  return (
    <React.Fragment>
      <DataProvider
        autoProvide={input !== ''}
        provider={fetchObjectTypeChoices(openServerErrorDialog)}
        providerArguments={[input]}
      >
        {({ data, busy }) => {
          const normalizedChoices = data || undefined;

          return (
            <Select
              {...props}
              choices={normalizedChoices}
              isClearable={true}
              loading={busy}
              getChoices={setInput}
              components={{
                Option: MultilineOption,
              }}
              filterOption={() => true}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};

export default ObjectTypeFinder;
