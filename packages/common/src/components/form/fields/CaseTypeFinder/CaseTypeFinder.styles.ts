// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles(() => {
  return {
    wrapper: {
      position: 'relative',
    },
    switchWrapper: {
      position: 'absolute',
      top: '-40px',
      right: '0%',
    },
  };
});
