// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import { DataProvider } from '@mintlab/ui/App/Abstract/DataProvider';
//@ts-ignore
import { isPopulatedArray } from '@mintlab/kitchen-sink/source';
import Select from '@mintlab/ui/App/Zaaksysteem/Select';
import { useTheme } from '@material-ui/styles';
import { Theme } from '@mintlab/ui/types/Theme';
import { createSharedStyleSheet } from '@mintlab/ui/App/Zaaksysteem/Select/Form/Shared.style';
import useServerErrorDialog from '@zaaksysteem/common/src/hooks/useServerErrorDialog';
import { FormFieldComponentType } from '../../types/Form2.types';
import { fetchDepartmentChoices } from './DepartmentFinder.library';
import { DepartmentFinderOptionType } from './DepartmentFinder.types';

export const DepartmentFinder: FormFieldComponentType<
  DepartmentFinderOptionType,
  any
> = ({ value, multiValue, styles, config, ...restProps }) => {
  const theme = useTheme<Theme>();
  const [ServerErrorDialog, openServerErrorDialog] = useServerErrorDialog();
  const { error } = restProps;
  const hideSpan = {
    '& span': {
      display: 'none',
    },
  };
  const stylesProp = {
    ...createSharedStyleSheet({
      theme,
      error,
    }),
    singleValue(base: any) {
      return {
        ...base,
        ...hideSpan,
      };
    },
    multiValueLabel(base: any) {
      return {
        ...base,
        ...hideSpan,
      };
    },
    ...((styles && styles) || {}),
  };
  return (
    <React.Fragment>
      <DataProvider
        autoProvide={true}
        provider={fetchDepartmentChoices(openServerErrorDialog)}
        providerArguments={[]}
      >
        {({ data, busy, provide }) => {
          const normalizedChoices = data || [];

          return (
            <Select
              {...restProps}
              choices={normalizedChoices}
              value={value}
              isClearable={true}
              loading={busy}
              getChoices={provide}
              disabled={!isPopulatedArray(normalizedChoices)}
              styles={stylesProp}
              isMulti={multiValue}
              multiValueLabelIcon={config?.multiValueLabelIcon}
            />
          );
        }}
      </DataProvider>
      {ServerErrorDialog}
    </React.Fragment>
  );
};
