# 🔌 `Wysiwyg` component

> Rich text editor facade for *React Draft Wysiwyg*.

## Usage

Pass in name and value props, and any configuration props from react-draft-wysiwyg.

## Example

    <Wysiwyg 
      name='test'
      value='test value'
      wrapperClassName='wrapper-class'
      toolbar={{
        options: ['inline', 'fontSize']
      }}
    >

## See also

- [`Wysiwyg` stories](/npm-mintlab-ui/storybook/?selectedKind=Zaaksysteem/Wysiwyg)
- [`Wysiwyg` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#zaaksysteem-wysiwyg)

## External resources

- [React Draft Wysiwyg](https://jpuri.github.io/react-draft-wysiwyg/#/docs)

