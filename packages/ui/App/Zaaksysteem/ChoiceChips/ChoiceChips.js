// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useReducer, useEffect } from 'react';
import { withStyles } from '@material-ui/styles';
import { asArray, callOrNothingAtAll } from '@mintlab/kitchen-sink/source';
import Chip from './library/Chip';
import { choiceChipsStylesheet } from './ChoiceChips.style';

const valueSelected = (values, value) => values.find(item => item === value);

const addSelectedValue = (values, value, multiSelect) =>
  multiSelect ? [...values, value] : [value];

const removeSelectedValue = (values, value, multiSelect) =>
  multiSelect ? values.filter(item => item !== value) : values;

const createFakeEvent = ({ name, value, type }) => ({
  target: {
    name,
    value,
  },
  type,
});

const containsFocussedChips = fields =>
  Object.values(fields).some(field => field);

function reducer(state, action) {
  const createNewState = chips => {
    return {
      ...state,
      chips,
      initial: false,
      hasFocussedChips: containsFocussedChips(chips),
    };
  };

  switch (action.type) {
    case 'focus':
      return createNewState({ ...state.chips, [action.value]: true });

    case 'blur':
      return createNewState({ ...state.chips, [action.value]: false });

    default:
      return state;
  }
}

const ChoiceChips = ({
  classes,
  choices,
  onChange,
  onBlur,
  name,
  error,
  multiSelect = false,
  value,
}) => {
  const selectedValues = asArray(value);
  const [state, dispatch] = useReducer(reducer, {
    initial: true,
    hasFocussedChips: false,
    chips: choices.reduce(
      (acc, choice) => ({
        ...acc,
        [choice.value]: false,
      }),
      {}
    ),
  });

  const handleChipClick = clickedValue => {
    const newSelectedValues = valueSelected(selectedValues, clickedValue)
      ? removeSelectedValue(selectedValues, clickedValue, multiSelect)
      : addSelectedValue(selectedValues, clickedValue, multiSelect);

    callOrNothingAtAll(onChange, [
      createFakeEvent({
        name,
        value: multiSelect ? newSelectedValues : newSelectedValues[0] || '',
        type: 'change',
      }),
    ]);
  };

  useEffect(() => {
    const { hasFocussedChips, initial } = state;

    if (!initial && !hasFocussedChips) {
      callOrNothingAtAll(onBlur, [
        createFakeEvent({
          name,
          type: 'blur',
        }),
      ]);
    }
  }, [state]);

  return (
    <ul className={classes.list}>
      {choices.map(item => {
        // Blur is delayed to allow next field to set focus first
        const delayedBlur = () => {
          setTimeout(() => dispatch({ type: 'blur', value: item.value }), 0);
        };

        return (
          <li className={classes.listItem} key={item.value}>
            <Chip
              label={item.label}
              renderIcon={item.renderIcon}
              onClick={() => handleChipClick(item.value)}
              onFocus={() => dispatch({ type: 'focus', value: item.value })}
              onBlur={delayedBlur}
              selected={valueSelected(selectedValues, item.value)}
              error={error}
            />
          </li>
        );
      })}
    </ul>
  );
};

export default withStyles(choiceChipsStylesheet)(ChoiceChips);
