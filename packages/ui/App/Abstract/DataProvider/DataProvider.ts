// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { useState, useEffect, useRef, memo } from 'react';
import deepEquals from 'fast-deep-equal';

type DataProviderPropsType<T> = {
  providerArguments?: any[];
  provider: (...args: any[]) => Promise<T>;
  autoProvide?: boolean;
  children: (props: {
    provide: any;
    busy: boolean;
    data: null | T;
    error: null | string;
  }) => any;
};

/* eslint complexity: [2, 5] */
function DataProvider<T>({
  children,
  autoProvide = true,
  provider,
  providerArguments = [],
}: DataProviderPropsType<T>) {
  const [state, setState] = useState({
    busy: false,
    data: null as null | T,
    error: null as null | string,
  });
  const { busy, data, error } = state;
  const previousProviderArguments = useRef(providerArguments);

  const getData = (...args: any[]) => {
    if (data === null && !busy) {
      const argumentsArray = Array.isArray(providerArguments)
        ? providerArguments
        : [providerArguments];

      setState({
        ...state,
        busy: true,
      });

      provider(...argumentsArray)
        .then(providerData =>
          setState({
            data: providerData,
            error: null,
            busy: false,
          })
        )
        .catch(error => {
          setState({
            ...state,
            error: error.toString(),
            busy: false,
          });
        });
    }
  };

  const getDataDelayed = (...args: any[]) => {
    return window.setTimeout(() => getData(...args), 100);
  };

  useEffect(() => {
    if (!deepEquals(previousProviderArguments.current, providerArguments)) {
      setState({
        data: null,
        busy: false,
        error: null,
      });
    }

    previousProviderArguments.current = providerArguments;
  }, [providerArguments]);

  useEffect(() => {
    let timeoutId: number;
    if (autoProvide && typeof provider === 'function' && data === null) {
      timeoutId = getDataDelayed();
    }

    return () => clearTimeout(timeoutId);
  }, [autoProvide, provider, data, providerArguments]);

  return children({
    data,
    busy,
    error,
    provide: getData,
  });
}

//@ts-ignore
const DataProviderMemo = memo(DataProvider);

export default DataProviderMemo as typeof DataProvider;
