// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import { default as Snackbar } from './Snackbar';
export * from './Snackbar';
export default Snackbar;
