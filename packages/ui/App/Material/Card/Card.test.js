// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
// eslint-disable-next-line import/no-unresolved
import toJson from 'enzyme-to-json';
// eslint-disable-next-line import/no-unresolved
import { shallow } from 'enzyme';
import { Card } from './Card';

/**
 * @test {Card}
 */
describe('The `Card` component', () => {
  test('renders the title & description prop in a `CardHeader` component', () => {
    const component = shallow(
      <Card title="Hello!" description="Hello!" classes={{}}>
        <div>Hello, World!</div>
      </Card>
    );

    expect(toJson(component)).toMatchSnapshot();
  });

  test('does not render the title & description prop in a `CardHeader` component', () => {
    const component = shallow(
      <Card classes={{}}>
        <div>Hello, World!</div>
      </Card>
    );

    expect(toJson(component)).toMatchSnapshot();
  });
});
