// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React, { useState } from 'react';
import classNames from 'classnames';
import MuiTooltip from '@material-ui/core/Tooltip';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import { withStyles } from '@material-ui/styles';

export const ActionFeedback = ({
  children,
  classes,
  title,
  type = 'default',
}) => {
  const [open, setOpen] = useState(false);

  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };

  return (
    <ClickAwayListener onClickAway={handleClose}>
      <div>
        <MuiTooltip
          PopperProps={{
            disablePortal: true,
          }}
          classes={{
            tooltip: classNames(classes.all, classes[type]),
            popper: classes.popper,
          }}
          onClose={handleClose}
          open={open}
          disableFocusListener
          disableHoverListener
          disableTouchListener
          title={title}
        >
          {children({ handleOpen })}
        </MuiTooltip>
      </div>
    </ClickAwayListener>
  );
};

export default withStyles({})(ActionFeedback);
