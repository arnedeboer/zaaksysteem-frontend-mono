# 🔌 `Paper` component

> Facade for *Material-UI* `Paper`.

## Example

    <Paper>My content.</Paper>

## See also

- [`Paper` stories](/npm-mintlab-ui/storybook/?selectedKind=Material/Paper)
- [`Paper` API reference](/npm-mintlab-ui/documentation/consumer/identifiers.html#material-paper)

## External resources

- [*Material-UI* `Paper` API](https://material-ui.com/api/paper/)

