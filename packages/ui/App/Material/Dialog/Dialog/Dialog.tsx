// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MuiDialog from '@material-ui/core/Dialog';
import { addScopeAttribute } from '../../../library/addScope';
import { useDialogStyles } from './Dialog.style';

type DialogPropsType = {
  open: boolean;
  classes?: { [key in 'paper']: string };
  scope?: string;
  onClose?: () => void;
  ref?: any;
  disableBackdropClick?: boolean;
  container?: React.ReactInstance | null;
};

export const Dialog: React.ComponentType<DialogPropsType> = React.forwardRef(
  ({ open = false, scope, children, classes, container, ...rest }, ref) => {
    const defaultClasses = useDialogStyles();

    const containerProps = container
      ? {
          container,
          style: { position: 'absolute' },
          BackdropProps: { style: { position: 'absolute' } },
        }
      : {};

    return (
      //@ts-ignore
      <MuiDialog
        ref={ref}
        open={open}
        classes={classes || defaultClasses}
        {...addScopeAttribute(scope, 'dialog')}
        {...containerProps}
        {...rest}
      >
        {children}
      </MuiDialog>
    );
  }
);

Dialog.displayName = 'Dialog';
