// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import MUIDialogTitle from '@material-ui/core/DialogTitle';
import { addScopeAttribute, addScopeProp } from '../../../library/addScope';
import Icon, { IconNameType } from '../../Icon/Icon';
import { H6 } from '../../Typography';
import Button from '../../Button/Button';
import { useDialogTitleStyles } from './DialogTitle.style';

export type DialogTitlePropsType = {
  icon?: React.ElementType | IconNameType;
  id?: string;
  onCloseClick?: () => void;
  elevated?: boolean;
  title: string;
  scope?: string;
};

/* eslint complexity: [2, 7] */
export const DialogTitle: React.ComponentType<DialogTitlePropsType> = ({
  icon,
  title,
  id,
  scope,
  elevated,
  onCloseClick,
}) => {
  const classes = useDialogTitleStyles();

  return (
    <MUIDialogTitle
      id={id}
      disableTypography={true}
      {...addScopeAttribute(scope, 'title')}
      classes={{
        root: elevated ? classes.rootElevated : classes.rootNormal,
      }}
    >
      {icon && typeof icon === 'string' && (
        <span className={classes.icon}>
          <Icon>{icon as IconNameType}</Icon>
        </span>
      )}

      {icon !== undefined && typeof icon !== 'string' && icon}

      <H6 classes={{ root: classes.typography }}>{title}</H6>

      {onCloseClick && (
        <div className={classes.closeButton}>
          <Button
            action={onCloseClick}
            presets={['default', 'icon']}
            {...addScopeProp(scope, 'close')}
          >
            close
          </Button>
        </div>
      )}
    </MUIDialogTitle>
  );
};
