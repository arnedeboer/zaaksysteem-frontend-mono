// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

import React from 'react';
import Typography from '@material-ui/core/Typography';

/**
 * @see /npm-mintlab-ui/storybook/?selectedKind=Material/Typography&selectedStory=Body1
 *
 * @param {Object} props
 * @param {*} props.children
 * @param {Object} [props.classes]
 * @param {string} [props.id]
 * @return {ReactElement}
 */
export const Body1 = ({ children, classes, id }) => (
  <Typography variant="body1" classes={classes} id={id}>
    {children}
  </Typography>
);
