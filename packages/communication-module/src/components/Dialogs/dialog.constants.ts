// This file is part of Zaaksysteem, which is released under the EUPL.
// See file LICENSE for full license details.

export const ADD_THREAD_TO_CASE_DIALOG = 'ADD_THREAD_TO_CASE_DIALOG';
export const IMPORT_MESSAGE_DIALOG = 'IMPORT_MESSAGE_DIALOG';
